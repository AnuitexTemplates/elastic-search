﻿using System;

namespace ElasticSearch.Sample.Web.Models
{
    public class HomeSearchModel
    {
        public string ProductName { get; set; }
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 1000;
        public DateTime OrderDateFrom { get; set; }
        public DateTime OrderDateTo { get; set; }
    }
}
