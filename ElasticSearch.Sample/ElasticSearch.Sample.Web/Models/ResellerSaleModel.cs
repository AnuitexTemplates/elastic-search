﻿using Nest;
using Newtonsoft.Json;
using System;

namespace ElasticSearch.Sample.Web.Models
{
    [ElasticsearchType(Name = "resellersales")]
    public class ResellerSaleModel
    {
        [Text]
        public string ProductName { get; set; }
        [Text]
        public string ResellerName { get; set; }
        [Date]
        public DateTime OrderDate { get; set; }
        [Text]
        public string ProductDescription { get; set; }
        [Text]
        public string SalesOrderNumber { get; set; }
        [Binary]
        [JsonProperty("binary")]
        public string LargePhoto { get; set; }
    }
}
