﻿using System;
using System.Collections.Generic;

namespace ElasticSearch.Sample.Web.Models
{
    public class HomeIndexModel
    {
        public IEnumerable<ResellerSaleModel> ResellerSales = new List<ResellerSaleModel>();
        public string ProductName { get; set; }
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 5;
        public long TotalPages { get; set; }
        public DateTime OrderDateFrom { get; set; } = new DateTime(2010, 1, 1);
        public DateTime OrderDateTo { get; set; } = DateTime.Now;
    }
}
