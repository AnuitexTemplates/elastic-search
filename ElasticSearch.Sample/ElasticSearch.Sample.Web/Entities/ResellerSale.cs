﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Dapper.Contrib.Extensions;
using Nest;

namespace ElasticSearch.Sample.Web.Entities
{
    [Dapper.Contrib.Extensions.Table("FactResellerSalesXL_CCI")]
    public class ResellerSale
    {
        [Column(name: "ProductKey")]
        public long ProductKey { get; set; }
        [Column(name: "OrderDateKey")]
        public long OrderDateKey { get; set; }
        [Column(name: "DueDateKey")]
        public long DueDateKey { get; set; }
        [Column(name: "ShipDateKey")]
        public long ShipDateKey { get; set; }
        [Column(name: "ResellerKey")]
        public long ResellerKey { get; set; }
        [Column(name: "EmployeeKey")]
        public long EmployeeKey { get; set; }
        [Column(name: "PromotionKey")]
        public long PromotionKey { get; set; }
        [Column(name: "CurrencyKey")]
        public long CurrencyKey { get; set; }
        [Column(name: "SalesTerritoryKey")]
        public long SalesTerritoryKey { get; set; }
        [Column(name: "SalesOrderNumber")]
        public string SalesOrderNumber { get; set; }
        [Column(name: "SalesOrderLineNumber")]
        public long SalesOrderLineNumber { get; set; }
        [Column(name: "UnitPrice")]
        public decimal UnitPrice { get; set; }
        [Column(name: "DiscountAmount")]
        public long DiscountAmount { get; set; }
        [Column(name: "TotalProductCost")]
        public long TotalProductCost { get; set; }
        [Column(name: "SalesAmount")]
        public long SalesAmount { get; set; }
        [Column(name: "TaxAmt")]
        public long TaxAmount { get; set; }
        [Column(name: "OrderDate")]
        public DateTime OrderDate { get; set; }
        [Column(name: "DueDate")]
        public DateTime DueDate { get; set; }
        [Column(name: "ShipDate")]
        public DateTime ShipDate { get; set; }
        [Column(name: "Freight")]
        public long Freight { get; set; }
        [Column(name: "CustomerPONumber")]
        public string CustomerPONumber { get; set; }
        [Column(name: "CarrierTrackingNumber")]
        public string CarrierTrackingNumber { get; set; }

        [Computed]
        [Nested]
        public virtual Product Product { get; set; }
        [Computed]
        [Nested]
        public virtual Reseller Reseller { get; set; }
    }
}
