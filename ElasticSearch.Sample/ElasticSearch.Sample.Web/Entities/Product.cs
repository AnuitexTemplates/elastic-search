﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ElasticSearch.Sample.Web.Entities
{
    [Dapper.Contrib.Extensions.Table("DimProduct")]
    public class Product
    {
        [Column("ProductKey")]
        public long ProductKey { get; set; }
        [Column("ProductSubcategoryKey")]
        public string ProductSubcategoryKey { get; set; }
        [Column("StandardCost")]
        public decimal StandardCost { get; set; }
        [Column("EnglishProductName")]
        public string EnglishProductName { get; set; }
        [Column("LargePhoto")]
        public byte[] LargePhoto { get; set; }
        [Column("EnglishDescription")]
        public string EnglishDescription { get; set; }

    }
}
