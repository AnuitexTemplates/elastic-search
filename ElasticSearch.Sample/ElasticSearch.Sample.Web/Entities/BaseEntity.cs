﻿using System;
namespace ElasticSearch.Sample.Web.Entities
{
    public abstract class BaseEntity
    {
        public long Id { get; set; }
        public DateTime CreationDate { get; set; }
        protected BaseEntity()
        {
            CreationDate = DateTime.UtcNow;
        }
    }
}
