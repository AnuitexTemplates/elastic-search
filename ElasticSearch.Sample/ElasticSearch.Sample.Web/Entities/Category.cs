﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElasticSearch.Sample.Web.Entities
{
    [Dapper.Contrib.Extensions.Table("DimProductCategory")]
    public class Category
    {
        [Column(name: "ProductCategoryKey")]
        public string Id { get; set; }
        [Column(name: "ProductCategoryAlternateKey")]
        public string AlternateKey { get; set; }
        [Column(name: "EnglishProductCategoryName")]
        public string Name { get; set; }
    }
}
