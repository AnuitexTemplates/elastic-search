﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ElasticSearch.Sample.Web.Entities
{
    [Dapper.Contrib.Extensions.Table("DimProductSubcategory")]
    public class SubCategory
    {
        [Column(name: "ProductSubcategoryKey")]
        public string Id { get; set; }
        [Column(name: "ProductSubcategoryAlternateKey")]
        public string AlternateKey { get; set; }
        [Column(name: "EnglishProductSubcategoryName")]
        public string Name { get; set; }
        [Column(name: "ProductCategoryKey")]
        public string CategoryId { get; set; }
    }
}