﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElasticSearch.Sample.Web.Entities
{
    [Dapper.Contrib.Extensions.Table("DimReseller")]
    public class Reseller
    {
        [Column(name: "RResellerKey")]
        public long Id { get; set; }
        [Column(name: "GeographyKey")]
        public long GeographyKey { get; set; }
        [Column(name: "ResellerAlternateKey")]
        public string AlternateKey { get; set; }
        [Column(name: "Phone")]
        public string Phone { get; set; }
        [Column(name: "BusinessType")]
        public string BusinessType { get; set; }
        [Column(name: "ResellerName")]
        public string ResellerName { get; set; }
        [Column(name: "NumberEmployees")]
        public int NumberEmployees { get; set; }
        [Column(name: "AnnualRevenue")]
        public decimal AnnualRevenue { get; set; }
        [Column(name: "YearOpened")]
        public int YearOpened { get; set; }
    }
}
