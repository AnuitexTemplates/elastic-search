﻿using Elasticsearch.Net;
using ElasticSearch.Sample.Web.Models;
using ElasticSearch.Sample.Web.Repositories;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElasticSearch.Sample.Web.Services
{
    public class IndexingService : IIndexingService
    {
        private IResellerSalesRepository _resellerSalesRepository;
        private IElasticClient _elasticClient;
        private IHostingEnvironment _hostingEnvironment;
        private ILogger _logger;

        public IndexingService(IResellerSalesRepository resellerSalesRepository, IElasticClient elasticClient,
            IHostingEnvironment hostingEnvironment, ILogger<IndexingService> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _hostingEnvironment = hostingEnvironment ?? throw new ArgumentNullException(nameof(hostingEnvironment));
            _resellerSalesRepository = resellerSalesRepository ?? throw new ArgumentNullException(nameof(resellerSalesRepository));
            _elasticClient = elasticClient ?? throw new ArgumentNullException(nameof(elasticClient));
        }

        public async Task IndexAsync()
        {
            //await IndexResellerSalesAsync();
        }

        public async Task IndexResellerSalesAsync()
        {
            try
            {
                //await _elasticClient.DeleteByQueryAsync<ResellerSaleModel>(q => q.MatchAll());
                var page = 405;
                var count = 1000;
                while (page < 1001)
                {
                    var resellerSalesPartial = await _resellerSalesRepository.GetListAsync(count, page);
                    var result = _elasticClient.IndexMany(resellerSalesPartial.Select(x => new ResellerSaleModel()
                    {
                        ProductDescription = x.Product.EnglishDescription,
                        ProductName = x.Product.EnglishProductName,
                        SalesOrderNumber = x.SalesOrderNumber,
                        OrderDate = x.OrderDate,
                        ResellerName = x.Reseller.ResellerName,
                        LargePhoto = Convert.ToBase64String(x.Product.LargePhoto)
                    }));
                    page++;
                }
                var request = new BulkRequest()
                {
                    Refresh = Refresh.True,
                    Operations = new List<IBulkOperation>()
                };

                _elasticClient.Refresh("resellersalesindex");
                _logger.LogInformation("Index was successfully created!");
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception, "Error was occured: Method:IndexResellerSalesAsync");
                if (_hostingEnvironment.IsDevelopment())
                {
                    throw;
                }
            }
        }
    }
}
