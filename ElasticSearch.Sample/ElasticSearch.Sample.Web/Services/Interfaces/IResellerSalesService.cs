﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ElasticSearch.Sample.Web.Entities;
using ElasticSearch.Sample.Web.Models;

namespace ElasticSearch.Sample.Web.Services
{
    public interface IResellerSalesService
    {
        Task<(IEnumerable<ResellerSaleModel>, long total)> Search(HomeSearchModel searchModel);
    }
}