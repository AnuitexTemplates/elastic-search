﻿using System.Threading.Tasks;

namespace ElasticSearch.Sample.Web.Services
{
    public interface IIndexingService
    {
        Task IndexAsync();
        Task IndexResellerSalesAsync();
    }
}