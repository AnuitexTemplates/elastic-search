﻿using ElasticSearch.Sample.Web.Models;
using ElasticSearch.Sample.Web.Repositories;
using Nest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ElasticSearch.Sample.Web.Services
{
    public class ResellerSalesService : IResellerSalesService
    {
        private readonly IResellerSalesRepository _resellerSalesRepository;
        private IElasticClient _elasticClient;

        public ResellerSalesService(IResellerSalesRepository resellerSalesRepository,
            IElasticClient elasticClient)
        {
            _resellerSalesRepository = resellerSalesRepository ?? throw new ArgumentNullException(nameof(resellerSalesRepository));
            _elasticClient = elasticClient ?? throw new ArgumentNullException(nameof(elasticClient));
        }

        public async Task<(IEnumerable<ResellerSaleModel>, long total)> Search(HomeSearchModel homeSearchModel)
        {
            var mustQuerySelectors = new List<QueryContainer>();

            var searchContainer = new QueryContainerDescriptor<ResellerSaleModel>()
                .Bool(b => b
                    .Should(
                        s => s.Match(m => m.Query(homeSearchModel.ProductName).Field(f => f.ProductName).Boost(1)),
                        s => s.Match(m => m.Query(homeSearchModel.ProductName).Field(f => f.ResellerName).Boost(0.8)),
                        s => s.Match(m => m.Query(homeSearchModel.ProductName).Field(f => f.SalesOrderNumber).Boost(1)),
                        s => s.Match(m => m.Query(homeSearchModel.ProductName).Field(f => f.ProductDescription).Boost(0.3))
            ).Filter(f => f
                .DateRange(n => n
                .Field(p => p.OrderDate)
                .GreaterThanOrEquals(Nest.DateMath.Anchored(homeSearchModel.OrderDateFrom))
                .LessThanOrEquals(Nest.DateMath.Anchored(homeSearchModel.OrderDateTo))
            )
            ));

            mustQuerySelectors.Add(searchContainer);

            var searchDescriptor = new SearchDescriptor<ResellerSaleModel>()
                .Query(x => searchContainer)
                .From((homeSearchModel.PageNumber - 1) * homeSearchModel.PageSize)
                .Size(homeSearchModel.PageSize);
            var response = await _elasticClient.SearchAsync<ResellerSaleModel>(searchDescriptor);


            var resellerSales = response.Documents;
            return (resellerSales, response.Total);
        }
    }
}
