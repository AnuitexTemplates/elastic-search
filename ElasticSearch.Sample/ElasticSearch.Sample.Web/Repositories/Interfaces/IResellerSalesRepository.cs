﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ElasticSearch.Sample.Web.Entities;

namespace ElasticSearch.Sample.Web.Repositories
{
    public interface IResellerSalesRepository : IDisposable
    {
        Task<IEnumerable<ResellerSale>> GetListAsync(int count, int pageNumber);
        Task<int> CountAsync();
    }
}