﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using ElasticSearch.Sample.Web.Entities;

namespace ElasticSearch.Sample.Web.Repositories
{
    public class ResellerSalesRepository : IDisposable, IResellerSalesRepository
    {
        private readonly SqlConnection _sqlConnection;

        public ResellerSalesRepository(AppSettings appSettings)
        {
            _sqlConnection = new SqlConnection(appSettings.ConnectionString ?? throw new ArgumentNullException(nameof(appSettings.ConnectionString)));
        }

        public async Task<int> CountAsync()
        {
            return await _sqlConnection.ExecuteAsync("SELECT COUNT(SalesOrderLineNumber) FROM FactResellerSalesXL_CCI");
        }

            public async Task<IEnumerable<ResellerSale>> GetListAsync(int count, int pageNumber)
        {
            var sql = @"SELECT RN, SalesOrderLineNumber, SalesOrderNumber,SalesAmount,ProductKey,OrderDateKey,DueDateKey,ShipDateKey,ResellerKey,EmployeeKey,CurrencyKey,
SalesTerritoryKey,UnitPrice,DiscountAmount,TotalProductCost,TaxAmt,Freight,Freight,CarrierTrackingNumber,CustomerPONumber,OrderDate,DueDate,ShipDate
 ,RResellerKey
 ,[GeographyKey]
      ,[ResellerAlternateKey]
      ,[Phone]
      ,[BusinessType]
      ,[ResellerName]
      ,[NumberEmployees]
      ,[AnnualRevenue]
      ,[YearOpened],
      PProductKey,
      ProductSubcategoryKey,
      StandardCost,
      EnglishProductName,
      LargePhoto,
      EnglishDescription 
FROM (SELECT rsxl.[ProductKey]
      ,rsxl.SalesOrderLineNumber
      ,rsxl.[OrderDateKey]
      ,rsxl.[DueDateKey]
      ,rsxl.[ShipDateKey]
      ,rsxl.[ResellerKey]
      ,rsxl.[EmployeeKey]
      ,rsxl.[PromotionKey]
      ,rsxl.[CurrencyKey]
      ,rsxl.[SalesTerritoryKey]
      ,rsxl.[SalesOrderNumber]
      ,rsxl.[UnitPrice]
      ,rsxl.[DiscountAmount]
      ,rsxl.[TotalProductCost]
      ,rsxl.[SalesAmount]
      ,rsxl.[TaxAmt]
      ,rsxl.[Freight]
      ,rsxl.[CarrierTrackingNumber]
      ,rsxl.[CustomerPONumber]
      ,rsxl.[OrderDate]
      ,rsxl.[DueDate]
      ,rsxl.[ShipDate]
      ,r.[ResellerKey] as RResellerKey
      ,r.[GeographyKey]
      ,r.[ResellerAlternateKey]
      ,r.[Phone]
      ,r.[BusinessType]
      ,r.[ResellerName]
      ,r.[NumberEmployees]
      ,r.[AnnualRevenue]
      ,r.[YearOpened],
      p.ProductKey as PProductKey,
      p.ProductSubcategoryKey,
      p.StandardCost,
      p.EnglishProductName,
      p.LargePhoto,
      p.EnglishDescription 
        ,ROW_NUMBER() OVER ( ORDER BY SalesOrderLineNumber) RN
     FROM [AdventureWorksDW2016_EXT].[dbo].[FactResellerSalesXL_CCI] rsxl 
  INNER JOIN DimReseller r ON r.ResellerKey = rsxl.ResellerKey
  INNER JOIN DimProduct p ON p.ProductKey = rsxl.ProductKey
  ) T
WHERE RN > @from AND RN < @to";
            return await _sqlConnection.QueryAsync<ResellerSale,Reseller,Product, ResellerSale >(sql,
                (resellerSale, reseller, product) =>
                {
                    resellerSale.Product = product;
                    resellerSale.Product.ProductKey = resellerSale.ProductKey;
                    resellerSale.Reseller = reseller;
                    resellerSale.Reseller.Id = resellerSale.ResellerKey;
                    return resellerSale;
                }
              ,new
            {
                from=count*(pageNumber-1),
                to=count*(pageNumber-1) + count+1
            }, splitOn: "RResellerKey,PProductKey");
        }

        public void Dispose()
        {
            _sqlConnection.Dispose();
        }
    }
}
