﻿using ElasticSearch.Sample.Web.Models;
using ElasticSearch.Sample.Web.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace ElasticSearch.Sample.Web.Controllers
{
    public class HomeController : Controller
    {
        private IResellerSalesService _resellerSalesService;

        public HomeController(IResellerSalesService resellerSalesService)
        {
            _resellerSalesService = resellerSalesService ?? throw new ArgumentNullException(nameof(resellerSalesService));
        }

        public async Task<IActionResult> Index()
        {
            var model = new HomeIndexModel();
            var searchModel = new HomeSearchModel()
            {
                PageNumber = model.PageNumber,
                PageSize = model.PageSize,
                ProductName = model.ProductName
            };
            (var resellerSales, var total) = await _resellerSalesService.Search(searchModel);
            model.ResellerSales = resellerSales;
            model.TotalPages = total / model.PageSize - 1;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Index(HomeIndexModel model)
        {
            var searchModel = new HomeSearchModel()
            {
                PageNumber = model.PageNumber,
                PageSize = model.PageSize,
                ProductName = model.ProductName,
                OrderDateFrom = model.OrderDateFrom,
                OrderDateTo = model.OrderDateTo
            };
            (var resellerSales, var total) = await _resellerSalesService.Search(searchModel);
            model.ResellerSales = resellerSales;
            model.TotalPages = total / model.PageSize - 1;
            return View(model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
