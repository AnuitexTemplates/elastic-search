﻿using Microsoft.Extensions.Configuration;

namespace ElasticSearch.Sample.Web
{
    public class AppSettings
    {
        public readonly string ElasticSearchEndpoint;
        public readonly string ConnectionString;
        public AppSettings(IConfiguration configuration)
        {
            var configurationSection = configuration.GetSection("Environments");
            ElasticSearchEndpoint = configurationSection.GetValue<string>(nameof(ElasticSearchEndpoint));
            ConnectionString = configurationSection.GetValue<string>(nameof(ConnectionString));
        }
    }
}
