﻿using ElasticSearch.Sample.Web.Models;
using Microsoft.Extensions.DependencyInjection;
using Nest;
using System;

namespace ElasticSearch.Sample.Web.Common
{
    public static class ElasticsearchExtensions
    {
        public static void AddElasticsearch(
            this IServiceCollection services, AppSettings appSettings)
        {
            var url = appSettings.ElasticSearchEndpoint;
            var defaultIndex = "resellersalesindex";

            var settings = new ConnectionSettings(new Uri(url))
                .DefaultIndex(defaultIndex);

            var client = new ElasticClient(settings);
            var response = client.Ping();
            if (!response.IsValid)
            {
                throw new ApplicationException();
            }
            services.AddSingleton<IElasticClient>(client);

            CreateIndex(client, defaultIndex);
        }

        private static void CreateIndex(IElasticClient client, string indexName)
        {
            var settings = new IndexSettings { NumberOfReplicas = 1, NumberOfShards = 5 };
            var indexConfig = new IndexState
            {
                Settings = settings
            };
            var isExist = (client.IndexExists(indexName)).Exists;
            if (isExist)
            {
                return;
                //client.DeleteIndex(indexName);
            }

            var createIndexResponse = client.CreateIndex(indexName, c => c
                .Settings(s => s
                .NumberOfShards(3)
                     .NumberOfReplicas(1))
                .Mappings(m => m
                    .Map<ResellerSaleModel>(x => x
                        .AutoMap()
                    )
                )
            );
        }
    }
}
